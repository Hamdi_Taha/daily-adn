import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import SignIn from './components/FormSignIn';
import UsersTable from './components/TableUsers';
import AddUser from './components/AddUser';


function App() {
  return (
   <BrowserRouter>
   <Routes>
    <Route path="/" element={<SignIn/>}></Route>
    <Route path="/table" element={<UsersTable/>}></Route>
    <Route path="/create/:id?" element={<AddUser/>}></Route>


   </Routes>
   </BrowserRouter>
  );
}

export default App;
