import { createSlice } from "@reduxjs/toolkit";
import { userList } from "../data/Data";

const UserSlice = createSlice({
  name: "users",
  initialState: userList,
  reducers: {
    addUser: (state, action) => {
      state.push(action.payload);
    },
    updateUser:(state,action)=>{
      const {id, name,Travailaujourdhui, Travailhier}=action.payload;
      const uu =state.find(user=>user.id === id);
      if(uu){
        uu.name = name;
        uu.Travailaujourdhui= Travailaujourdhui;
        uu.Travailhier=Travailhier;
      }

      console.log(action,"++++")
    }
  },
});

// Action creators are generated for each case reducer function
// export const {  } = counterSlice.actions
export const { addUser,updateUser } = UserSlice.actions;
export default UserSlice.reducer;
