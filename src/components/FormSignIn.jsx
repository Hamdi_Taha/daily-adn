import { useNavigate } from "react-router";
import "../index.css";

function SignIn() {

    const navigate = useNavigate();

    const handleClick = () => {
        navigate("/table")
    }
  return (
    <div className="header-signin no-scroll">
      <div className="form-position">
        <form className="form">
          <div class="mb-3">
            <label for="Email1" class="form-label">
              Email address
            </label>
            <input type="email" class="form-control" id="Email1" />
          </div>
          <div class="mb-3">
            <label for="Password1" class="form-label">
              Password
            </label>
            <input type="password" class="form-control" id="Password1" />
          </div>
        <button onClick={handleClick} type="submit" class="btn btn-primary">
          Submit
        </button>
        </form>
      </div>
    </div>
  );
}

export default SignIn;
