import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addUser, updateUser } from "../redux/UserReducer";
import { useNavigate, useParams } from "react-router";

function AddUser() {
  const { id } = useParams();

  const users = useSelector((state) => state.users);
  const existingUser = users.filter((elt) => elt.id === id);
  console.log(existingUser, "existingUser");
  const [name, setName] = useState(existingUser[0]?.name);
  const [futureDate, setFutureDate] = useState(
    existingUser[0]?.Travailaujourdhui
  );
  const [pastDate, setPastDate] = useState(existingUser[0]?.Travailhier);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  console.log(users, "users");
  const handleSubmit = (event) => {
    event.preventDefault();
    if (id) {
      dispatch(
        updateUser({
          id: id,
          name: name,
          Travailaujourdhui: futureDate,
          Travailhier: pastDate,
        })
      );
    } else {
      dispatch(
        addUser({
          id: users[users.length - 1].id + 1,
          name: name,
          Travailaujourdhui: futureDate,
          Travailhier: pastDate,
        })
      );
    }
    navigate("/table");
  };
  return (
    <div className="d-flex w-100 vh-100 justify-content-center align-items-center">
      <div className="w-50 border bg-secondary text-white p-5">
        <form onSubmit={handleSubmit}>
          <div>
            <label>Nom:</label>
            <input
              type="text"
              name="name"
              className="form-control"
              onChange={(e) => setName(e.target.value)}
              value={name}
            />
          </div>
          <div>
            <label>Travail d'hier</label>
            <textarea
              type="text"
              className="form-control"
              onChange={(e) => setPastDate(e.target.value)}
              value={pastDate}
            />
          </div>
          <div>
            <label>Travail d'aujourd’hui</label>
            <textarea
              type="text"
              className="form-control"
              onChange={(e) => setFutureDate(e.target.value)}
              value={futureDate}
            />
          </div>
          <br />
          <button className="btn btn-info">Submit</button>
        </form>
      </div>
    </div>
  );
}

export default AddUser;
