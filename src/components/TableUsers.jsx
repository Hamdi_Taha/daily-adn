import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
function UsersTable() {
  const users = useSelector((state) => state.users);
  return (
    <div>
      <h2 className="d-flex justify-content-center mt-3">Daily report</h2>
      <div className="d-flex justify-content-center mt-3">
        <Link to="/create" className="btn btn-success my-3">
          Ajouter
        </Link>
      </div>

      <table className="table">
        <thead>
          <tr>
            <th>Nom</th>
            <th className="text-info">Ce que j’ai fait hier </th>
            <th className="text-info">Ce que je veux faire aujourd’hui</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr key={index}>
              <td>{user.name}</td>
              <td>
                {" "}
                {user.Travailhier.split("\n").map((item, i) => (
                  <div key={i}>{item}</div>
                ))}
              </td>
              <td>
                {user.Travailaujourdhui.split("\n").map((item, i) => (
                  <div key={i}>{item}</div>
                ))}
              </td>
              <td>

                <Link to={`/create/${user.id}`} className="btn btn-sm btn-primary me-2">Modifier</Link>
                <button className="btn btn-sm btn-danger">Supprimer</button>

              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default UsersTable;
